using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    public enum EnumStatusVenda
    {
        [Description("Aguardando Pagamento")]
        AguardandoPagamento,

        [Description("Pagamento Aprovado")]
        PagamentoAprovado,

        [Description("Enviado Para Transportadora")]
        EnviadoParaTransportadora,

        [Description("Entregue")]
        Entregue,

        [Description("Cancelada")]
        Cancelada
    }
}