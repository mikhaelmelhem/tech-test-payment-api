using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        public int Id { get; set; }
        public int IdVendedor { get; set; }
        public string NomeVendedor { get; set; }
        public DateTime DataVenda { get; set; }
        public EnumStatusVenda Status { get; set; }
        public string CodigoPedido { get; set; }
        public string NomeItem { get; set; }
        public int IdItem { get; set; }
    }
}