using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ItemController : ControllerBase
    {

        private readonly PagamentoContext _context;

        public ItemController(PagamentoContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var item = _context.Items.Find(id);
            if (item == null)
                return NotFound();
            return Ok(item);
        }

        [HttpGet("ListasTodosOsItens")]
        public IActionResult ListarItens()
        {
            var itens = _context.Items.ToList();
            return Ok(itens);
        }

        [HttpPost]
        public IActionResult Criar(Item item)
        {
            _context.Items.Add(item);
            _context.SaveChanges();
            return Ok(item);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Item item)
        {
            var itemLocalizar = _context.Items.Find(id);

            if (itemLocalizar == null)
                return NotFound();

            itemLocalizar.Nome = item.Nome;

            _context.Items.Update(itemLocalizar);
            _context.SaveChanges();
            return Ok(itemLocalizar);
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id){
            var itemDeletar = _context.Items.Find(id);

            if (itemDeletar == null)
                return NotFound();

            _context.Items.Remove(itemDeletar);
            _context.SaveChanges();
            return Ok($"O item de id {id} foi excluido do bando com sucesso!");
        }

    }
}