using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly PagamentoContext _context;

        public VendaController(PagamentoContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {

            var venda = _context.Vendas.Find(id);
            if (venda == null)
                return NotFound();

            return Ok(venda);
        }

        [HttpPost]
        public IActionResult CadastrarVenda(Venda venda)
        {
            var vendaAdd = venda;
            vendaAdd.Status = 0;
            _context.Vendas.Add(venda);
            _context.SaveChanges();
            return Ok(venda);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Venda venda)
        {
            string resposta = "";
            var vendaAtualizar = _context.Vendas.Find(id);
            int statusAntigo = (int)vendaAtualizar.Status;
            int statusNovo = (int)venda.Status;

            // if (statusNovo != 0 || statusNovo != 1 || statusNovo != 2)
            //     throw new ArgumentException("Esse status não pode ser alterado ou não existe!");

            if (vendaAtualizar == null)
                return NotFound();

            switch (statusAntigo)
            {
                case 0:
                    if ((statusNovo != 1 && statusNovo != 4))
                    {
                        throw new ArgumentException($"Alteração de status não autorizada!");
                    }
                    break;
                case 1:
                    if ((statusNovo != 2 && statusNovo != 4))
                    {
                        throw new ArgumentException("Alteração de status não autorizada!");
                    }
                    break;
                case 2:
                    if ((statusNovo != 3))
                    {
                        throw new ArgumentException("Alteração de status não autorizada!");
                    }
                    break;
            }

            vendaAtualizar.IdVendedor = venda.IdVendedor;
            vendaAtualizar.NomeVendedor = venda.NomeVendedor;
            vendaAtualizar.DataVenda = venda.DataVenda;

            if (statusAntigo == 0 || statusAntigo == 1 || statusAntigo == 2)
            {
                vendaAtualizar.Status = venda.Status;
                resposta = "Dados atualizados com sucesso!";
            }
            else { resposta = "Dados atualizados com sucesso, porém o status não pôde ser alterado!"; }

            vendaAtualizar.CodigoPedido = venda.CodigoPedido;
            vendaAtualizar.NomeItem = venda.NomeItem;
            vendaAtualizar.IdItem = venda.IdItem;

            _context.Vendas.Update(vendaAtualizar);
            _context.SaveChanges();
            return Ok(resposta);
        }

    }
}