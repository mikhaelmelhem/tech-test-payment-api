using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly PagamentoContext _context;

        public VendedorController(PagamentoContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var vendedor = _context.Vendedores.Find(id);
            if (vendedor == null)
                return NotFound();
            return Ok(vendedor);
        }

        [HttpGet("ListasTodosOsVendedores")]
        public IActionResult ListarVendedores()
        {
            var vendedores = _context.Vendedores.ToList();
            return Ok(vendedores);
        }

        [HttpPost]
        public IActionResult Criar(Vendedor vendedor)
        {
            _context.Vendedores.Add(vendedor);
            _context.SaveChanges();
            return Ok(vendedor);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Vendedor vendedores)
        {
            var vendedorLocalizar = _context.Vendedores.Find(id);

            if (vendedorLocalizar == null)
                return NotFound();

            vendedorLocalizar.Cpf = vendedores.Cpf;
            vendedorLocalizar.Nome = vendedores.Nome;
            vendedorLocalizar.Email = vendedores.Email;
            vendedorLocalizar.Telefone = vendedores.Telefone;

            _context.Vendedores.Update(vendedorLocalizar);
            _context.SaveChanges();
            return Ok(vendedorLocalizar);
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var vendedorDeletar = _context.Vendedores.Find(id);

            if (vendedorDeletar == null)
                return NotFound();

            _context.Vendedores.Remove(vendedorDeletar);
            _context.SaveChanges();
            return Ok($"O vendedor de id {vendedorDeletar} foi excluido do bando com sucesso!");
        }
    }
}